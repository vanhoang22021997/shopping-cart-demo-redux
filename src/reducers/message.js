import * as types from './../constants/ActionType'
import * as msg from './../constants/Message'


let initialState = msg.MSG_WELLCOME



const message = (state = initialState, action) => {
  
    switch (action.type){
        case types.CHANGE_MESSAGE:
          return action.message
        default : return state
    }
}

export default message