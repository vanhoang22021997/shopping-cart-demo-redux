import * as types from "./../constants/ActionType";

let data = JSON.parse(localStorage.getItem("cart"));

let initialState = data ? data : [];

const products = (state = initialState, action) => {
  const { product, quantity } = action;
  switch (action.type) {
    case types.ADD_TO_CART:
      let indexAdd = state.findIndex(item => {
        return item.product.name === product.name;
      });
      if (indexAdd !== -1) {
        state[indexAdd].quantity++;
      } else {
        state.push({
          product,
          quantity
        });
      }
      localStorage.setItem("cart", JSON.stringify(state));
      return [...state];

    case types.DELETE_TO_CART:
      let indexDelete = state.findIndex(item => {
        return item.product.name === action.cartName;
      });
      if (indexDelete !== -1) {
        state.splice(indexDelete, 1);
      }
      localStorage.setItem("cart", JSON.stringify(state));
      return [...state];

    case types.UPDATE_TO_CART:
      let indexUpdate = state.findIndex(item => {
        return item.product.name === action.product.name;
      });
      if (state[indexUpdate].quantity > 1) {
        state[indexUpdate].quantity += action.quantity;
      } else {
        state.splice(indexUpdate, 1);
      }
      localStorage.setItem("cart", JSON.stringify(state));
      return [...state];
    default:
      return state;
  }
};

export default products;
