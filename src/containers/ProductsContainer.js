import React, { Component } from "react";
import Products from "./../components/Products";
import Product from "./../components/Product";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import * as actions from "./../actions/index";

class ProductsContainer extends Component {
  render() {
    const { products } = this.props;
    return <Products>{this.showProduct(products)}</Products>;
  }
  showProduct = products => {
    const { onAddToCart, onChangeMessage } = this.props;
    let result = "";
    if (products.length > 0) {
      result = products.map((product, index) => {
        return (
          <Product key={index} product={product} onAddToCart={onAddToCart} onChangeMessage={onChangeMessage}/>
        );
      });
    }
    return result;
  };
}

// eslint-disable-next-line react/no-typos
ProductsContainer.propTypes = {
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired
    })
  ).isRequired
};

const mapStateToProps = state => {
  return {
    products: state.products
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onAddToCart: product => {
      dispatch(actions.addCart(product, 1));
    },
    onChangeMessage: message => {
      dispatch(actions.changeMessage(message));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductsContainer);
