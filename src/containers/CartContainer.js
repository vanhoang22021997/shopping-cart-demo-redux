import React, { Component } from "react";
import Cart from "./../components/Cart";
import CartItem from "./../components/CartItem";
import CartResult from "./../components/CartResult";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import * as message from "./../constants/Message";
import * as actions from "./../actions/index";

class CartContainer extends Component {
  render() {
    const { cart } = this.props;
    return (
      <Cart>
        {this.showCart(cart)}
        {this.showTotalAmount(cart)}
      </Cart>
    );
  }

  showCart = cart => {
    let result = (
      <tr>
        <td>{message.MSG_CART_EMPTY}</td>
      </tr>
    );
    if (cart.length > 0) {
      const { onDeleteCart, onChangeMessage, onUpdateCart } = this.props;
      result = cart.map((cart, index) => {
        return (
          <CartItem
            key={index}
            cartItem={cart}
            onDeleteCart={onDeleteCart}
            onChangeMessage={onChangeMessage}
            onUpdateCart={onUpdateCart}
          />
        );
      });
    }
    return result;
  };

  showTotalAmount = cart => {
    let result = (
      <tr>
        <td />
      </tr>
    );
    if (cart.length > 0) {
      result = <CartResult cart={cart} />;
    }
    return result;
  };
}

// eslint-disable-next-line react/no-typos
CartContainer.propTypes = {
  cart: PropTypes.arrayOf(
    PropTypes.shape({
      product: PropTypes.shape({
        id: PropTypes.number.isRequired
      }).isRequired,
      quantity: PropTypes.number.isRequired
    })
  ).isRequired,
  onDeleteCart: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
    cart: state.cart
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onDeleteCart: cartName => {
      dispatch(actions.deleteCart(cartName));
    },
    onUpdateCart: (product, quantity) =>{
      dispatch(actions.updateCart(product, quantity))
    },
    onChangeMessage: message => {
      dispatch(actions.changeMessage(message));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CartContainer);
