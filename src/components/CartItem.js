/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import * as msg from "./../constants/Message";
class CartItem extends Component {
  render() {
    const { cartItem } = this.props;
    return (
      <tr>
        <th scope="row">
          <img
            src={cartItem.product.image}
            alt=""
            className="img-fluid z-depth-0"
          />
        </th>
        <td>
          <h5>
            <strong>{cartItem.product.name}</strong>
          </h5>
        </td>
        <td>{cartItem.product.price}$</td>
        <td className="center-on-small-only">
          <span className="qty">{cartItem.quantity}</span>
          <div className="btn-group radio-group" data-toggle="buttons">
            <label
              className="btn btn-sm btn-primary
                                    btn-rounded waves-effect waves-light"
            >
              <a
                onClick={() => {
                  this.onUpdateCart(cartItem.product, -1);
                }}
              >
                —
              </a>
            </label>
            <label
              className="btn btn-sm btn-primary
                                    btn-rounded waves-effect waves-light"
            >
              <a
                onClick={() => {
                  this.onUpdateCart(cartItem.product, 1);
                }}
              >
                +
              </a>
            </label>
          </div>
        </td>
        <td>{this.showSubTotal(cartItem.quantity, cartItem.product.price)}$</td>
        <td>
          <button
            type="button"
            className="btn btn-sm btn-primary waves-effect waves-light"
            data-toggle="tooltip"
            data-placement="top"
            title=""
            data-original-title="Remove item"
            onClick={() => {
              this.onDeleteCart(cartItem.product.name);
            }}
          >
            X
          </button>
        </td>
      </tr>
    );
  }

  onDeleteCart = cartName => {
    const { onChangeMessage, onDeleteCart } = this.props;
    onDeleteCart(cartName);
    onChangeMessage(msg.MSG_DELETE_PRODUCT_IN_CART_SUCCESS);
  };

  showSubTotal = (quantity, price) => {
    return quantity * price;
  };

  onUpdateCart = (product, quantity) => {
    const { onChangeMessage, onUpdateCart } = this.props;
    onUpdateCart(product, quantity);
    onChangeMessage(msg.MSG_UPDATE_CART_SUCCESS);
  };
}

export default CartItem;
