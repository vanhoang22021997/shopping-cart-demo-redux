/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import * as msg from "./../constants/Message"
var randomstring = require("randomstring");
class Product extends Component {
  render() {
    let { product } = this.props;
    return (
      <div className="col-lg-4 col-md-6 mb-r">
        <div className="card text-center card-cascade narrower">
          <div className="view overlay hm-white-slight z-depth-1">
            <img src={product.image} className="img-fluid" alt="" />
            <a>
              <div className="mask waves-light waves-effect waves-light" />
            </a>
          </div>
          <div className="card-body">
            <h4 className="card-title">
              <strong>
                <a>{product.name}</a>
              </strong>
            </h4>
            <ul className="rating">
              <li>{this.showRating(product.rating)}</li>
            </ul>
            <p className="card-text">{product.description}</p>
            <div className="card-footer">
              <span className="left">{product.price}</span>
              <span className="right">
                <a
                  className="btn-floating blue-gradient"
                  data-toggle="tooltip"
                  data-placement="top"
                  title=""
                  data-original-title="Add to Cart"
                  onClick={() => {
                    this.onAddToCart(product)
                  }}
                >
                  <i className="fa fa-shopping-cart" />
                </a>
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
  showRating = rating => {
    let result = [];
    for (let index = 1; index <= rating; index++) {
      result.push(<i key={randomstring.generate()} className="fa fa-star" />);
    }
    for (let index = 1; index <= 5 - rating; index++) {
      result.push(<i key={randomstring.generate()} className="fa fa-star-o" />);
    }
    return result;
  };

  onAddToCart = (product) => {
    const { onAddToCart, onChangeMessage } = this.props;
    onAddToCart(product)
    onChangeMessage(msg.MSG_ADD_TO_CART_SUCCESS)
  }
}

export default Product;
