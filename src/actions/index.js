import * as types from "./../constants/ActionType";

export const addCart = (product, quantity) => {
  return {
    type: types.ADD_TO_CART,
    product,
    quantity
  };
};

export const changeMessage = (message) => {
  return {
    type: types.CHANGE_MESSAGE,
    message
  }
}

export const deleteCart = (cartName) => {
  return {
    type : types.DELETE_TO_CART,
    cartName
  }
}

export const updateCart = (product, quantity) => {
  return {
    type: types.UPDATE_TO_CART,
    product,
    quantity
  }
}